defmodule Test.MySchema do
  use Ecto.Schema
  import Ecto.Changeset

  schema "myschema" do
    field :myfield, :string

    timestamps()
  end
end

