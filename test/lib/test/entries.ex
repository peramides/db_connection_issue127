defmodule Test.Entries do
  alias Test.Repo
  alias Test.MySchema

  def list_entries do
    Repo.all(MySchema)
  end
end

